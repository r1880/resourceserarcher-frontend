import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FilterPipe } from './services/filter.pipe';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APIInterceptorService } from './services/apiinterceptor.service'; import { HeaderComponent } from './header/header.component'
import { InsertLanguageComponent } from './insert-language/insert-language.component'
import { InsertTechnologyComponent } from './insert-technology/insert-technology.component'
import { InsertResourceComponent } from './resources-components/insert-resource/insert-resource.component'
import { ResourcesListComponent } from './resources-components/resources-list/resources-list.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { SafePipe } from './safe.pipe';
import { UpdateResourceComponent } from './resources-components/update-resource/update-resource.component';
import {OrderListModule} from 'primeng/orderlist';
import {DataViewModule} from 'primeng/dataview';
import {AccordionModule} from 'primeng/accordion';
import {ButtonModule} from 'primeng/button';
import { InputTextModule} from 'primeng/inputtext';
import  {DropdownModule } from 'primeng/dropdown';
import {FileUploadModule} from 'primeng/fileupload';
import {CalendarModule} from 'primeng/calendar';
import {ListboxModule} from 'primeng/listbox'
import {MultiSelectModule} from 'primeng/multiselect';
import {TabMenuModule} from 'primeng/tabmenu';
import {MenubarModule} from 'primeng/menubar';
import {MenuModule} from 'primeng/menu';
import {PickListModule} from 'primeng/picklist';
import { LoginComponent } from './login/login.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { HashLocationStrategy, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { UsersComponent } from './users-components/users/users.component';
import { UpdateUserComponent } from './users-components/update-user/update-user.component';
import {RadioButtonModule} from 'primeng/radiobutton';
import { ResourcesSearchComponent } from './resources-components/resources-search/resources-search.component';
import {DatePipe} from '@angular/common';
import { MessagesModule } from 'primeng/messages';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { TabViewModule } from 'primeng/tabview';
import { DividerModule } from "primeng/divider";
import {ChartModule} from 'primeng/chart';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {CardModule} from 'primeng/card';
 
@NgModule({
  declarations: [
    AppComponent,
    InsertResourceComponent,
    HeaderComponent,
    InsertLanguageComponent,
    InsertTechnologyComponent,
    ResourcesListComponent,
    FilterPipe,
    ResourcesListComponent,
    HeaderComponent,
    SafePipe,
    UpdateResourceComponent,
    LoginComponent,
    ResetPasswordComponent,
    UsersComponent,
    UpdateUserComponent,
    ResourcesSearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    OrderListModule,
    DataViewModule,
    AccordionModule,
    ButtonModule,
    InputTextModule,
    FileUploadModule,
    CalendarModule,
    ListboxModule,
    DropdownModule,
    MultiSelectModule,
    TabMenuModule,
    MenubarModule,
    MenuModule,
    RadioButtonModule,
    PickListModule,
    ConfirmDialogModule,
    MessagesModule,
    TabViewModule,
    DividerModule,
    ChartModule,
    ProgressSpinnerModule,
    CardModule
      ],

  providers: [
    { provide: "BASE_API_URL", useValue: environment.apiUrl },
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: APIInterceptorService,
      multi: true,
    },
    FilterPipe,
    DatePipe,
    ConfirmationService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
