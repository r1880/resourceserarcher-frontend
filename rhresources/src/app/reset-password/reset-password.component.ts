import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../services/CommonService';
import { Login } from '../services/login/Login';
import { LoginService } from '../services/login/LoginService.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {


  loginService: LoginService;
  commonService: CommonService;
  activatedRoute: ActivatedRoute;
  router: Router;
  token: String;
  username: string;
  notEqual: boolean = false;

  pwdForm = new FormGroup({
    pwd: new FormControl(''),
    pwdbis: new FormControl('')
  });

  emailForm = new FormGroup({
    email: new FormControl('')
  });

  constructor(loginService: LoginService, commonService: CommonService, router: Router, activatedRoute: ActivatedRoute) {
    this.commonService = commonService;
    this.loginService = loginService;
    this.router = router;
    this.activatedRoute = activatedRoute;
  
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.token = params['token'];
      this.username = params['user'];

    });
  
  }





  emailSend() {
    if (this.emailForm.valid) {
      let username = this.emailForm.value["email"];
      this.loginService.sendEmailReset(username).subscribe(e => {
        console.log(e);
      }
      );
      this.router.navigate(['/login']);

    } else {
      this.commonService.getFormValidationErrors(this.emailForm);
    }
  }

  pwdReset() {
    if (this.pwdForm.valid) {
      let pwd = this.pwdForm.value["pwd"];
      let pwdBis = this.pwdForm.value["pwdbis"];
      if (pwd == pwdBis) {
        let user = new Login;
        user.username = this.username;
        user.pwd = pwd;
        this.loginService.resetPassword(user).subscribe(e => {
          console.log(e);
          this.router.navigate(['/login']);
        });
      } else {
        this.notEqual = true;
      }
      //  this.router.navigate(['resources']);

    } else {
      this.commonService.getFormValidationErrors(this.pwdForm);
    }
  }


}
