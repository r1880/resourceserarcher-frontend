import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertTechnologyComponent } from './insert-technology.component';

describe('InsertTechnologyComponent', () => {
  let component: InsertTechnologyComponent;
  let fixture: ComponentFixture<InsertTechnologyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsertTechnologyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertTechnologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
