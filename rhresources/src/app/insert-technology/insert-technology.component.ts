import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { LoginService } from '../services/login/LoginService.service';
import { Technology } from '../services/technology/Technology';
import { TechnologyService } from '../services/technology/technology.service';

@Component({
  selector: 'app-insert-technology',
  templateUrl: './insert-technology.component.html',
  styleUrls: ['./insert-technology.component.css']
})
export class InsertTechnologyComponent implements OnInit {

  private technologyService: TechnologyService;
  private router: Router;
  private loginService: LoginService;

  tech: string;
  technologies: Technology[];
  confirmationService: ConfirmationService;


  constructor(loginService: LoginService, technologyService: TechnologyService, router: Router, confirmationService: ConfirmationService
  ) {
    this.router = router;
    this.loginService = loginService;
    this.loginService.isLogged(this.router);
    this.technologyService = technologyService;
    this.confirmationService = confirmationService;
  }


  ngOnInit(): void {
    this.technologyService.getTechnologies().subscribe(res => {
      this.technologies = res
    });
  }

  add(): void {

    if (this.notNull(this.tech)) {
      let isPresent = false;
      if (this.technologies != undefined) {
        this.technologies.forEach((technology) => {
          if (technology.name == this.tech) {
            isPresent = true;
          }
        });
      } else {
        this.technologies = []
      }

      if (isPresent == false) {
        let technology = new Technology();
        technology.name = this.tech;
        this.technologyService.insertTechnology(technology).subscribe(l => this.technologies.push(technology));
        this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
          this.router.navigate(['/technologies']);
        });
        this.tech = "";
      }
    }
  }

  notNull(obj: any) {
    return obj != null && obj != undefined && obj != "";
  }

  remove(tech: Technology): void {
    this.technologyService.removeTechnology(tech).subscribe(l => this.technologies = this.technologies.filter(l => l.name != tech.name));
  }

  confirm(tech: Technology) {
    this.confirmationService.confirm({
      acceptLabel: "Si",
      message: '¿Eliminar ' + tech.name + ' de la lista de tecnologías?',
      accept: () => {
        this.remove(tech);
      }
    });
  }
}