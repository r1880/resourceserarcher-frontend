import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Login } from 'src/app/services/login/Login';
import { LoginService } from 'src/app/services/login/LoginService.service';
import { SharedUserService } from 'src/app/services/login/SharedUserService';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  private loginService: LoginService;
  private sharedUserService : SharedUserService;
  private router: Router;
  users: Login[];



  constructor(loginService: LoginService, router: Router,sharedUSerService : SharedUserService) {
    this.loginService = loginService;
    this.router = router;
    this.loginService.isLogged(this.router);
    this.router = router;
    this.sharedUserService=sharedUSerService;
  }

  ngOnInit(): void {
    this.loginService.getUsers().subscribe(users => {
      this.users = users;
    });
  }

  updateUser(user: Login) {
    this.sharedUserService.setUser(user);
    this.router.navigateByUrl('/update-user');
  }


}
