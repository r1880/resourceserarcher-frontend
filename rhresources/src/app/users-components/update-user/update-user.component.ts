import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/CommonService';
import { Login } from 'src/app/services/login/Login';
import { LoginService } from 'src/app/services/login/LoginService.service';
import { SharedUserService } from 'src/app/services/login/SharedUserService';

interface Role {
  name: string

}

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})


export class UpdateUserComponent implements OnInit {


  private loginService: LoginService;
  private router: Router;
  private sharedUserService: SharedUserService;
  private commonService: CommonService;
  user: Login;
  roles: string[] = ["USER", "ADMIN"];


  userForm = new FormGroup({
    name: new FormControl(''),
    surname: new FormControl(''),
    username: new FormControl(''),
    email: new FormControl(''),
    roleSelected: new FormControl('')
  });



  constructor(loginService: LoginService, router: Router, sharedUserService: SharedUserService, commonService: CommonService) {
    this.loginService = loginService;
    this.router = router;
    this.loginService.isLogged(this.router);
    this.router = router;
    this.sharedUserService = sharedUserService;
    this.commonService = this.commonService;

    this.sharedUserService.getUser().subscribe(user => {
      this.user = user;
    });

    if (this.user == null) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
    this.setValues();
  }

  setValues() {
    this.userForm.controls["name"].setValue(this.notNull(this.user.name) == true ? this.user.name : "");
    this.userForm.controls["surname"].setValue(this.notNull(this.user.surname) == true ? this.user.surname : "");
    this.userForm.controls["username"].setValue(this.notNull(this.user.username) == true ? this.user.username : "");
    this.userForm.controls["email"].setValue(this.notNull(this.user.email) == true ? this.user.email : "");
    this.userForm.controls["roleSelected"].setValue(this.notNull(this.user.role) == true ? this.user.role : "");

  }


  notNull(obj: any) {
    return obj != null && obj != undefined;
  }

  onSubmit() {

    if (this.userForm.valid) {
      let user = new Login();
      user.name = this.userForm.value["name"];
      user.surname = this.userForm.value["surname"];
      user.username = this.userForm.value["username"];
      user.email = this.userForm.value["email"];
      user.role = this.userForm.value["roleSelected"];


      this.loginService.updateUser(user).subscribe(e => { this.router.navigate(['/users']); });

    } else {
      this.commonService.getFormValidationErrors(this.userForm);
    }


  }

}
