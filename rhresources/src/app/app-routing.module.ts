import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InsertLanguageComponent } from './insert-language/insert-language.component';
import { InsertTechnologyComponent } from './insert-technology/insert-technology.component';
import { LoginComponent } from './login/login.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { InsertResourceComponent } from './resources-components/insert-resource/insert-resource.component';
import { ResourcesListComponent } from './resources-components/resources-list/resources-list.component';
import { ResourcesSearchComponent } from './resources-components/resources-search/resources-search.component';
import { UpdateResourceComponent } from './resources-components/update-resource/update-resource.component';
import { UpdateUserComponent } from './users-components/update-user/update-user.component';
import { UsersComponent } from './users-components/users/users.component';


const routes: Routes = [
 
  {path: 'resources', component: ResourcesListComponent},
  {path: 'insertResource', component: InsertResourceComponent},
  {path: 'languages', component: InsertLanguageComponent},
  {path: 'technologies', component: InsertTechnologyComponent},
  {path: 'update-resource', component: UpdateResourceComponent},
  {path: 'login', component: LoginComponent},
  {path: 'resetPassword', component: ResetPasswordComponent},
  {path: 'users', component: UsersComponent},
  {path: 'update-user', component: UpdateUserComponent},
  {path: 'resources-search', component: ResourcesSearchComponent},
  {path: '', redirectTo: '/resources', pathMatch: 'full'}
];

export const APP_ROUTING = RouterModule.forRoot(routes, {useHash: true});

@NgModule({
  imports: [APP_ROUTING],
  exports: [RouterModule]
})
export class AppRoutingModule { }
