import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { Language } from '../services/language/Langauge';
import { LanguageService } from '../services/language/language.service';
import { LoginService } from '../services/login/LoginService.service';


@Component({
  selector: 'app-insert-language',
  templateUrl: './insert-language.component.html',
  styleUrls: ['./insert-language.component.css']
})
export class InsertLanguageComponent implements OnInit {

  private languageService: LanguageService;
  private loginService : LoginService;
  lang: string;
  languages: Language[];
  router: Router
   confirmationService: ConfirmationService

  constructor(loginService: LoginService,languageService: LanguageService, router: Router, confirmationService: ConfirmationService) {
    this.loginService = loginService;
    this.router = router;
    this.loginService.isLogged(this.router);
    this.languageService = languageService;
    this.confirmationService=confirmationService;
  }


  ngOnInit(): void {

    this.loginService.isLogged(this.router);
    this.languageService.getLanguages().subscribe(res => {
      this.languages = res
    });
  }

  add(): void {

    let isPresent = false;
    if (this.notNull(this.lang)) {
      if (this.languages != undefined) {
        this.languages.forEach((language) => {
          if (language.name == this.lang) {
            isPresent = true;
          }
        });
      } else {
        this.languages = []
      }

      if (isPresent == false) {
        let language = new Language();
        language.name = this.lang;
        this.languageService.insertLanguage(language).subscribe(l => this.languages.push(language));
        this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
          this.router.navigate(['/languages']);
        });
        this.lang="";
      }
    }



  }

  notNull(obj: any) {
    return obj != null && obj != undefined && obj != "";
  }



  remove(lang: Language): void {
    this.languageService.removeLanguage(lang).subscribe(l => this.languages = this.languages.filter(l => l.name != lang.name));
  }


  confirm(lang: Language) {
    this.confirmationService.confirm({
        acceptLabel: "Si",
        message: '¿Eliminar '+lang.name+' de la lista de idiomas?',
        accept: () => {
          this.remove(lang);
        }
    });
}

}
