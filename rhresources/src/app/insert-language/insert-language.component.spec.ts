import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertLanguageComponent } from './insert-language.component';

describe('InsertLanguageComponent', () => {
  let component: InsertLanguageComponent;
  let fixture: ComponentFixture<InsertLanguageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsertLanguageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
