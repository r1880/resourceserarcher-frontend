import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  items: MenuItem[];
  items2: MenuItem[];

  router: Router;
  status: boolean;
  constructor(router: Router) {
    this.router = router;
  }

  ngOnInit(): void {
    this.status = this.loggedSatus();
    this.items = [
      {
        label: 'Dashboard',
        routerLink: ['/resources'],
        styleClass: 'menucus'
      },
       {
        label: 'Analítica',
        routerLink: ['/resources-search'],
        styleClass: 'menucus'
      },
       {
        label: 'Tecnologías',
        routerLink: ['/technologies'],
        styleClass: 'menucus'
      }, {
        label: 'Idiomas',
        routerLink: ['/languages'],
        styleClass: 'menucus'
      }, {
        label: 'Añadir recurso',
        routerLink: ['/insertResource'],
        styleClass: 'menucus'
      }
    ]

    this.items2 = [
      {



        label: 'Administrar usuarios', icon: 'pi pi-fw pi-pencil', routerLink: ['/users'],
        command: () => {
          this.goto();
        }
      },
      {
        label: 'Salir', icon: "pi pi-power-off", command: () => {
          this.exit();
        }
      }
    ]


  }

  exit() {
    localStorage.removeItem("token");
    this.router.navigateByUrl("/login");
  }

  goto() {
    this.router.navigate(['/users']);

  }

  loggedSatus(): boolean {
    if (localStorage.getItem("token") != null) {
      return true;
    } else {
      return false;
    }
  }

}
