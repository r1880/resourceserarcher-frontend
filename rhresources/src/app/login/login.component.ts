import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../services/CommonService';
import { Login } from '../services/login/Login';
import { LoginService } from '../services/login/LoginService.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private router: Router;


  loginForm = new FormGroup({
    username: new FormControl(''),
    pwd: new FormControl(''),
  });

  login: Login;
  loginService: LoginService;
  commonService: CommonService;

  constructor(commonService: CommonService, loginService: LoginService, router: Router) {
    this.commonService = commonService;
    this.loginService = loginService;
    this.router = router;
    if(localStorage.getItem("token")!=null){
      this.router.navigateByUrl('/');
    }

  }

  ngOnInit(): void {
  }


  onSubmit() {
    if (this.loginForm.valid) {
      this.login = new Login();
      var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; 
      var user= this.loginForm.value["username"] ;
      if(user.match(pattern)){
      this.login.email = user;
    }else{
      this.login.username =user;

    } 
      this.login.pwd = this.loginForm.value["pwd"];
      this.loginService.getToken(this.login).subscribe(e => {
        if (e !== undefined) {
          localStorage.setItem("token", e as string);
          this.router.navigateByUrl('/resources');
        }
      }
      );
      this.router.navigate(['resources']);

    } else {
      this.commonService.getFormValidationErrors(this.loginForm);
    }
  }
}
