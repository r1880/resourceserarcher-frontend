import { Injectable } from "@angular/core";
import { FormGroup, ValidationErrors } from "@angular/forms";

@Injectable({
    providedIn: 'root'
    
  })
  
  
  
  export class CommonService {


    getFormValidationErrors(form : FormGroup) {
        Object.keys(form.controls).forEach(key => {
    
          const controlErrors: ValidationErrors = form.get(key).errors;
          
          if (controlErrors != null) {
            form.get(key).markAsDirty();
            Object.keys(controlErrors).forEach(keyError => {
              console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
            });
          }
        });
      }
    
    

  }