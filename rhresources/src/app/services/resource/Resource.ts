import { Language } from '../language/Langauge';
import { Technology } from '../technology/Technology';
import { CV } from './cv';

export class Resource {

    name: string;
    surname: string;
    email: string;
    id: string;
    salary: number;
    incorporationDate: string;
    profile: string;
    yearsOfExperience: number;
    technologies: Technology[];
    languages: Language[];
    phoneNumber : string;
    cvs : CV[];

}