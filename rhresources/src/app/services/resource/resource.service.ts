import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, PipeTransform } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, count, map, tap } from 'rxjs/operators';
import { Resource } from './Resource';
import { CV } from './cv';

@Injectable({
  providedIn: 'root'
})
export class ResourceService {

  private resourcePath = 'resources';
  private analyticPath = 'analitycs';

  private filesPath = 'file';

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  

  constructor(private http: HttpClient) {

  }

  uploadFile(files: File[], id: string) {

    const formData: FormData = new FormData();
    for (let i = 0; i < files.length; i++) {
      formData.append('doc', files[i]);
    }
    const url = `${this.filesPath}/${id}`
    return this.http
      .post(url, formData)
      .pipe(        catchError(this.handleError<[]>('fileupload', []))

      );
  }

  downloadFile(cv: CV) {
  
    const url = `${this.filesPath}/${cv.id}`
    return this.http
      .get(url, { responseType: "blob"} ).pipe(
        catchError(this.handleError<any[]>('fileUpload', []))
      )

  }

  deleteFile(cv: CV) {
    const httpOptionsDelete = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', }),
      body: cv
    };
    return this.http
      .delete(this.filesPath, httpOptionsDelete).pipe(
        catchError(this.handleError<any[]>('fileUpload', []))
      )

  }


  getResources(): Observable<Resource[]> {
    return this.http.get<Resource[]>(this.resourcePath)
      .pipe(
        catchError(this.handleError<Resource[]>('getResources', []))
      );
  }


  getCount(id: String, source: string): Observable<number> {
    let a: any = {"id":id,"source":source };
    const url = `${this.resourcePath}` + "/count" ;
    return this.http.post<number>(url, a,this.httpOptions)
      .pipe(
        catchError(this.handleError<number>('getResources',))
      );
  }

  getYearEvolution(techs :string): Observable<number> {
    const url = `${this.analyticPath}` + "/techyears" ;
    return this.http.post<number>(url, [techs],this.httpOptions)
      .pipe(
        catchError(this.handleError<number>('getResources',))
      );
  }

  getYearEvolutionGroup(techs :string[]): Observable<number> {
    const url = `${this.analyticPath}` + "/techyears" ;
    return this.http.post<number>(url, techs,this.httpOptions)
      .pipe(
        catchError(this.handleError<number>('getResources',))
      );
  }

  getStadistics(techs :string[]): Observable<number> {
    const url = `${this.analyticPath}` + "/stadistics" ;
    return this.http.post<number>(url, techs,this.httpOptions)
      .pipe(
        catchError(this.handleError<number>('getResources',))
      );
  }

  getResource(id: String): Observable<Resource> {
    const url = `${this.resourcePath}/${id}`;
    return this.http.get<Resource>(url).pipe(
      tap(_ => console.log(`fetched resource with id=${id}`)),
      catchError(this.handleError<Resource>(`getHero id=${id}`))
    );
  }

  updateResource(resource: Resource) {
    return this.http.put(this.resourcePath, resource, this.httpOptions).pipe(
      tap(_ => console.log(`updated resource id=${resource.id}`)),
      catchError(this.handleError<Resource>('resourceUpdate'))
    );
  }

  insertResource(resource: Resource) {
    return this.http.post(this.resourcePath, resource, this.httpOptions).pipe(
      tap(_ => console.log(`updated resource id=${resource.id}`)),
      catchError(this.handleError<Resource>('resourceInsert'))
    );
  }

  findResource(resource: Resource): Observable<Resource[]> {
    const url = this.resourcePath.concat("/find");
    return this.http.post<Resource[]>(url, resource, this.httpOptions).pipe(
      tap(_ => console.log(`updated resource id=${resource.id}`)),
      catchError(this.handleError<Resource[]>('resourceInsert'))
    );
  }


  removeResource(resource: Resource) {
    const url =this.resourcePath.concat("/resource/").concat(resource.id);
    console.log("deleting")
    return this.http.delete(url, this.httpOptions).pipe(
      tap(_ => console.log(`deleted resource id=${resource.id}`)),
      catchError(this.handleError<Resource[]>('resourceInsert'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.log(error);
      //console.error(error); // log to console

      return of(result as T);
    };
  }
}
