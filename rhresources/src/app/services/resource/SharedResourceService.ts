import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Resource } from './Resource';

@Injectable({
  providedIn: 'root'
})
export class SharedResourceService {

  resource: BehaviorSubject<Resource> = new BehaviorSubject(null);

  constructor() {
  }

  getResource(): Observable<Resource> {
    return this.resource.asObservable();
  }

  setResource(resource: Resource) {
    this.resource.next(resource);
  }
}

