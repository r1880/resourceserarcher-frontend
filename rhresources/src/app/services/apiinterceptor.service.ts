import { Inject, Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

providers: [
  { provide: "BASE_API_URL", useValue: environment.apiUrl }
]


@Injectable({
  providedIn: 'root'
})


export class APIInterceptorService implements HttpInterceptor {

  token: string;
  router : Router

  constructor(@Inject('BASE_API_URL') private baseUrl: string, router : Router) {
    this.router=router;
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.token = localStorage.getItem("token");
    let auth = "Bearer " + this.token;
    const apiReq = req.clone({
      url: `${this.baseUrl}/${req.url}`, setHeaders: {
        Authorization: auth
      }
    });

    return next.handle(apiReq).pipe(tap(() => { },
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status == 401 || err.status == 403 ) {
            localStorage.clear();
            this.router.navigate(['login']);
          }
          return;
        }
      }));
  }

}