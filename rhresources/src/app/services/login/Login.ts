
export class Login {

  name: string;
  surname: string;
  status: boolean;
  email: string;
  username: string;
  pwd: string;
  role: string;
}