import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Login } from './Login';

@Injectable({
  providedIn: 'root'
})
export class SharedUserService {

  login: BehaviorSubject<Login> = new BehaviorSubject(null);

  constructor() {
  }

  getUser(): Observable<Login> {
    return this.login.asObservable();
  }

  setUser(login: Login) {
    this.login.next(login);
  }
}

