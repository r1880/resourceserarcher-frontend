import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Login } from './Login';


@Injectable({
  providedIn: 'root'
})
export class LoginService {


  private authPath = 'auth';
  private resetPath = 'resetPassword';
  private usersPath = 'users';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', })
  };


  constructor(private http: HttpClient) {


  }

  isLogged(router: Router) {

    let token = localStorage.getItem("token")
    if (token == null) {
      router.navigate(["login"]);
    }
  }


  getToken(login: Login) {
    const headers = new HttpHeaders({
      'content-type': 'application/json'
    });
    return this.http.post(this.authPath, login, { headers, responseType: 'text' }).pipe(
      tap(),
      catchError(this.handleError<Login>('resourceInsert'))
    );
  }

  sendEmailReset(user: String) {
    const headers = new HttpHeaders({
      'content-type': 'application/json'
    });
    return this.http.put(this.authPath, user, { headers }).pipe(
      tap(),
      catchError(this.handleError<Login>('resourceInsert'))
    );
  }

  resetPassword(user: Login) {
    const headers = new HttpHeaders({
      'content-type': 'application/json'
    });
    return this.http.put(this.resetPath, user, { headers }).pipe(
      tap(),
      catchError(this.handleError<Login>('resourceInsert'))
    );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error + " ERROR LOGIN"); // log to console

      return of(result as T);
    };
  }

  getUsers(): Observable<Login[]> {
    return this.http.get<Login[]>(this.usersPath)
      .pipe(
        catchError(this.handleError<any[]>('getUsers', []))
      );
  }

  removeUser(user: Login) {
    const httpOptionsDelete = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', }),
      body: user
    };

  }

  insertUser(user: Login) {
    return this.http.post(this.usersPath , user, this.httpOptions)
    .pipe(
      catchError(this.handleError<any[]>('getLanguages', []))
      );
      
  }

  updateUser(user: Login) {
    return this.http.put(this.usersPath,user, this.httpOptions).pipe(
      tap(_ => console.log(`updated resource id=${user.username}`)),
      catchError(this.handleError<Login>('resourceUpdate'))
    );
  }


}
