import { HttpClient, HttpHeaders, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable,of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Language } from './Langauge';



@Injectable({
  providedIn: 'root'
  
})



export class LanguageService {

  private languagePath = 'languages';

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', })
  };
  
  constructor(private http: HttpClient) {


  }

  getLanguages(): Observable<Language[]> {
    return this.http.get<Language[]>(this.languagePath)
      .pipe(
        catchError(this.handleError<any[]>('getLanguages', []))
      );
  }

  removeLanguage(language: Language) {
    const httpOptionsDelete = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', }),
      body :language  
    };
   
   return  this.http.delete(this.languagePath,httpOptionsDelete)
      .pipe(
        catchError(this.handleError<any[]>('removeLanguage', []))
      );
  }

  insertLanguage(language: Language) {
    return this.http.post(this.languagePath, language, this.httpOptions)
    .pipe(
      catchError(this.handleError<any[]>('getLanguages', []))
      );
      
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: T): Observable<T> => {

      console.log(error); // log to console

      return of(result as T);
    };
  }
}

