import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Technology } from './Technology';

@Injectable({
  providedIn: 'root'
})
export class TechnologyService {

  
  private technologyPath = 'technologies';

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) {


  }

  getTechnologies(): Observable<Technology[]> {
    return this.http.get<Technology[]>(this.technologyPath)
      .pipe(
        catchError(this.handleError<Technology[]>('getTechnologies', []))
      );
  }

  removeTechnology(tech: Technology) {
    const httpOptionsDelete = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', }),
      body :tech  
    };
     return this.http.delete(this.technologyPath,httpOptionsDelete)
      .pipe(
        catchError(this.handleError<Technology[]>('removeTechnology', []))
      );
  }

  insertTechnology(language: Technology) {
  return   this.http.post(this.technologyPath, language, this.httpOptions)
    .pipe(
      catchError(this.handleError<any[]>('insertTechnology', []))
    );
  }

  


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console

      return of(result as T);
    };
  }
}
