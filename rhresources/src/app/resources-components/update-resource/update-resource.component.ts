import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { CommonService } from 'src/app/services/CommonService';
import { Language } from 'src/app/services/language/Langauge';
import { LanguageService } from 'src/app/services/language/language.service';
import { LoginService } from 'src/app/services/login/LoginService.service';
import { CV } from 'src/app/services/resource/cv';
import { Resource } from 'src/app/services/resource/Resource';
import { ResourceService } from 'src/app/services/resource/resource.service';
import { SharedResourceService } from 'src/app/services/resource/SharedResourceService';
import { Technology } from 'src/app/services/technology/Technology';
import { TechnologyService } from 'src/app/services/technology/technology.service';

@Component({
  selector: 'app-update-resource',
  templateUrl: './update-resource.component.html',
  styleUrls: ['./update-resource.component.css']
})
export class UpdateResourceComponent implements OnInit {


  fileToUpload: File[] = null;
  files: FileList;
  calendarDate: Date;

  langSelect: FormControl = new FormControl();
  techSelect: FormControl = new FormControl()
  resourceForm = new FormGroup({
    id: new FormControl(),
    name: new FormControl(),
    surname: new FormControl(''),
    email: new FormControl(''),
    salary: new FormControl(''),
    incorporationDate: new FormControl(''),
    profile: new FormControl(''),
    phoneNumber: new FormControl(''),
    yearsOfExperience: new FormControl(''),
    techSelect: this.techSelect,
    langSelect: this.langSelect
  });


  private router: Router;
  private technologyService: TechnologyService;
  private languageService: LanguageService;
  private resourceService: ResourceService;
  private sharedResourceService: SharedResourceService;
  private loginService: LoginService;
  private commonService: CommonService;
  private confirmationService: ConfirmationService;
  deletedCvs: CV[] = [];
  technologies: Technology[];
  languages: Language[];
  resource: Resource;
  minDate: Date = new Date();

  datePipe: DatePipe;


  constructor(loginService: LoginService, technologyService: TechnologyService, languageService: LanguageService, resourceService: ResourceService, sharedResourceService: SharedResourceService, router: Router, commonService: CommonService,
    datePipe: DatePipe, confirmationService: ConfirmationService) {

    this.router = router;
    this.loginService = loginService;
    this.loginService.isLogged(this.router);
    this.commonService = commonService;
    this.technologyService = technologyService;
    this.languageService = languageService;
    this.resourceService = resourceService;
    this.sharedResourceService = sharedResourceService;
    this.datePipe = datePipe;
    this.technologyService.getTechnologies().subscribe(t => this.technologies = t);
    this.languageService.getLanguages().subscribe(l => this.languages = l);
    this.sharedResourceService.getResource().subscribe(r => {
      this.resource = r;

    });
    if (this.resource == null) {
      this.router.navigate(['/']);
    }
    this.confirmationService = confirmationService;
  }

  ngOnInit(): void {


    this.setValues();
  }

  handleFileInput(files: FileList) {
    this.files = files;
  }

  setValues() {
    this.resourceForm.controls["name"].setValue(this.notNull(this.resource.name) == true ? this.resource.name : "");
    this.resourceForm.controls["surname"].setValue(this.notNull(this.resource.surname) == true ? this.resource.surname : "");
    this.resourceForm.controls["id"].setValue(this.notNull(this.resource.id) == true ? this.resource.id : "");
    this.resourceForm.controls["email"].setValue(this.notNull(this.resource.email) == true ? this.resource.email : "");
    this.resourceForm.controls["salary"].setValue(this.notNull(this.resource.salary) == true ? this.resource.salary : "");
    this.resourceForm.controls["incorporationDate"].setValue(this.notNull(this.resource.incorporationDate) == true ? this.resource.incorporationDate : "");
    this.resourceForm.controls["profile"].setValue(this.notNull(this.resource.profile) == true ? this.resource.profile : "");
    this.resourceForm.controls["phoneNumber"].setValue(this.notNull(this.resource.phoneNumber) == true ? this.resource.phoneNumber : "");
    this.resourceForm.controls["yearsOfExperience"].setValue(this.notNull(this.resource.yearsOfExperience) == true ? this.resource.yearsOfExperience : "");


    this.techSelect.setValue(this.resource.technologies);
    this.langSelect.setValue(this.resource.languages);

  }

  notNull(obj: any) {
    return obj != null && obj != undefined;
  }

  buildFromForm(): Resource {
    let resource = new Resource();
    resource.name = this.resourceForm.value["name"];
    resource.surname = this.resourceForm.value["surname"];
    resource.salary = Number(this.resourceForm.value["salary"]);
    resource.id = this.resource.id;
    resource.email = this.resourceForm.value["email"];
    console.log();
    try {
      resource.incorporationDate = this.datePipe.transform(new Date(this.resourceForm.value["incorporationDate"]), "dd-MM-yyyy");

    } catch (error) {
      resource.incorporationDate =this.resourceForm.value["incorporationDate"]
    }
    resource.profile = this.resourceForm.value["profile"];
    resource.technologies = this.techSelect.value;
    resource.phoneNumber = this.resourceForm.value["phoneNumber"];
    resource.languages = this.langSelect.value;
    resource.yearsOfExperience = Number(this.resourceForm.value["yearsOfExperience"]);

    return resource;
  }

  onUploadOwn(files: FileList) {
    this.files = files;
  }


  update(resource: Resource) {

    this.deleteCVs();

    this.resourceService.updateResource(resource).subscribe(e => { console.log(e) });

    if (this.files != null) {
      this.fileToUpload = []
      resource.cvs = []
      for (let i = 0; i < this.files.length; i++) {
        this.fileToUpload.push(this.files.item(i));

      }
      if (this.fileToUpload.length > 0) {
        this.resourceService.uploadFile(this.fileToUpload, resource.id).subscribe(s => { console.log("uploaded") }, e => { console.log(e) }, () => { this.router.navigate(['/']) });
      } else {
        this.router.navigate(['/']);
      }
    }
  }


deleteCVs(){
  if (this.deletedCvs != undefined && this.deletedCvs.length > 0) {
    this.deletedCvs.forEach(cv => {
      this.resourceService.deleteFile(cv).subscribe(e => { console.log(e) });
    });
  }
}

confirm() {
  if (this.resourceForm.valid) {

    let resource: Resource = this.buildFromForm();

    this.confirmationService.confirm({
      acceptLabel: "Si",
      message: '¿Actualizar al recurso ' + resource.name + ' ' + resource.surname + '?',
      accept: () => {
        this.update(resource);
      }
    });
  } else {
    this.commonService.getFormValidationErrors(this.resourceForm);

  }

}

remove(cv: CV){
  let index = this.resource.cvs.indexOf(cv);
  if (index > -1) {
    this.resource.cvs.splice(index, 1);
    this.deletedCvs.push(cv);
  }
}


}
