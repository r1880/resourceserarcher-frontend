import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Language } from '../../services/language/Langauge';
import { LanguageService } from '../../services/language/language.service';
import { Resource } from '../../services/resource/Resource';
import { ResourceService } from '../../services/resource/resource.service';
import { Technology } from '../../services/technology/Technology';
import { TechnologyService } from '../../services/technology/technology.service';
import { Router } from "@angular/router"
import { CommonService } from 'src/app/services/CommonService';
import { LoginService } from 'src/app/services/login/LoginService.service';
import { stringify } from '@angular/compiler/src/util';
import { DatePipe } from '@angular/common';
import { ConfirmationService } from 'primeng/api';


@Component({
  selector: 'app-insert-resource',
  templateUrl: './insert-resource.component.html',
  styleUrls: ['./insert-resource.component.css']
})
export class InsertResourceComponent implements OnInit {


  incorporationDate: FormControl = new FormControl(Validators.required)
  fileToUpload: File[] = null;
  files: FileList;

  langSelect: FormControl = new FormControl()
  techSelect: FormControl = new FormControl()
  resourceForm = new FormGroup({
    name: new FormControl(''),
    surname: new FormControl(''),
    email: new FormControl(''),
    identifier: new FormControl(''),
    salary: new FormControl(''),
    profile: new FormControl(''),
    phoneNumber: new FormControl(''),
    yearsOfExperience: new FormControl(''),
    incorporationDate: new FormControl(''),
    techSelect: this.techSelect,
    langSelect: this.langSelect
  });

  technologies: Technology[]
  languages: Language[]
  router: Router;
  private technologyService: TechnologyService;
  private languageService: LanguageService;
  private resourceService: ResourceService;
  private loginService: LoginService;
  private confirmationService: ConfirmationService;

  minDate: Date = new Date();
  private commonService: CommonService;
  private datePipe: DatePipe;

  constructor(loginService: LoginService, technologyService: TechnologyService, languageService: LanguageService, resourceService: ResourceService, router: Router, commonService: CommonService, datePipe: DatePipe, confirmationService: ConfirmationService) {

    this.router = router;
    this.loginService = loginService;
    this.loginService.isLogged(this.router);
    this.commonService = commonService;
    this.technologyService = technologyService;
    this.languageService = languageService;
    this.resourceService = resourceService;
    this.technologyService.getTechnologies().subscribe(t => this.technologies = t);
    this.languageService.getLanguages().subscribe(l => this.languages = l);
    this.datePipe = datePipe;
    this.confirmationService = confirmationService;
  }

  ngOnInit(): void {

  }

  onUploadOwn(files: FileList) {
    this.files = files;
  }

  buildRespurceFromForm(): Resource {

    let resource = new Resource();
    resource.name = this.resourceForm.value["name"];
    resource.surname = this.resourceForm.value["surname"];
    resource.salary = Number(this.resourceForm.value["salary"]);
    resource.id = this.resourceForm.value["identifier"];
    resource.email = this.resourceForm.value["email"];
    resource.incorporationDate = this.datePipe.transform(this.resourceForm.value["incorporationDate"], "dd-MM-yyyy");
    resource.profile = this.resourceForm.value["profile"];
    resource.technologies = this.techSelect.value;
    resource.phoneNumber = this.resourceForm.value["phoneNumber"];
    resource.languages = this.langSelect.value;
    resource.yearsOfExperience = Number(this.resourceForm.value["yearsOfExperience"]);

    return resource;
  }

  insert(resource: Resource) {


    this.resourceService.insertResource(resource).subscribe(e => {
      if (this.files != null) {
        this.fileToUpload = []
        resource.cvs = []
        for (let i = 0; i < this.files.length; i++) {
          this.fileToUpload.push(this.files.item(i));
         }
        if (this.fileToUpload.length > 0)
          this.resourceService.uploadFile(this.fileToUpload, resource.id).subscribe(e => { console.log(e) });
      }
      this.router.navigate(['/']);

    });


  }
  confirm() {
    if (this.resourceForm.valid) {

      let resource: Resource = this.buildRespurceFromForm();
      this.confirmationService.confirm({
        acceptLabel: "Si",
        message: '¿Insertar recurso ' + resource.name + ' ' + resource.surname + '?',
        accept: () => {
          this.insert(resource);
        }
      });
    } else {
      this.commonService.getFormValidationErrors(this.resourceForm);

    }
  }



}








