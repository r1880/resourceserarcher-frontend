import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertResourceComponent } from './insert-resource.component';

describe('InsertResourceComponent', () => {
  let component: InsertResourceComponent;
  let fixture: ComponentFixture<InsertResourceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsertResourceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
