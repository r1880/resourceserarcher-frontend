import { AfterViewInit, Component, Input, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { Language } from 'src/app/services/language/Langauge';
import { LoginService } from 'src/app/services/login/LoginService.service';
import { Resource } from 'src/app/services/resource/Resource';
import { ResourceService } from 'src/app/services/resource/resource.service';
import { Technology } from 'src/app/services/technology/Technology';
import { TechnologyService } from 'src/app/services/technology/technology.service';

import { UIChart } from 'primeng/chart';


@Component({
  selector: 'app-resources-search',
  templateUrl: './resources-search.component.html',
  styleUrls: ['./resources-search.component.css']
})
export class ResourcesSearchComponent implements OnInit {

  private resourceService: ResourceService;
  private technologyService: TechnologyService;
  private loginService: LoginService;
  private router: Router;

  technologies: Technology[];
  resources: Resource[];
  languages: Language[];
  techNames: String[] = [];
  langTarget: Language[];
  techTarget: Technology[];
  yearsTarget: number;
  profileTarget: string;
  data: any;
  options: any;
  basicData: any = {};
  basicDataGroup: any;
  basicStadistic: any;
  basicStadisticGroup: any;


  

  counter: number[] = new Array();
  colors = new Array();
  loading: boolean = true;
  comparedCharts: boolean = false;
  @Input('ngModel')
  selectedTechnologiesSalaryTech: Technology[];
  @Input('ngModel')
  selectedGroupTechnologiesSalaryTech: Technology[];
  @Input('ngModel')
  selectedTechnologiesStadistic: Technology[];
  @Input('ngModel')
  selectedTechnologiesStadisticGroup : Technology[];
 


  constructor(loginService: LoginService, resourceService: ResourceService, router: Router, technologyService: TechnologyService) {
    this.loginService = loginService;
    this.router = router;
    this.loginService.isLogged(this.router);
    this.resourceService = resourceService;
    this.router = router;
    this.technologyService = technologyService;


  }


  ngOnInit(): void {

    this.technologyService.getTechnologies().subscribe(t => {
      this.technologies = t
    },error =>{},()=>{
      this.generateTechsPie();
    });
  }

 

  generateTechsPie() {


    this.technologies.forEach(t => {
      this.techNames.push(t.name);
      this.resourceService.getCount(t.name, "technologies._id").subscribe(i => {
        this.counter.push(i);
      }, error => { }, () => {
        console.log("done")
        this.generateColors(this.technologies.length);
        this.data = {
          labels: this.techNames,
          datasets: [
            {
              data: this.counter,
              backgroundColor: this.colors,
              hoverBackgroundColor: this.colors
            }],
        };

        this.options = {
          tooltips: {
            callbacks: {
              label: function (tooltipItem, data) {
                let allData = data.datasets[tooltipItem.datasetIndex].data;
                let tooltipLabel = data.labels[tooltipItem.index];
                let tooltipData = allData[tooltipItem.index];
                let total = 0;
                for (let i in allData) {
                  total += allData[i];
                }
                let tooltipPercentage = Math.round((tooltipData / total) * 100);
                return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
              }
            }
          }
        };
        this.loading = false;

      })
    });





  }



  generateColors(length: number) {
    for (let i = 0; i < length; i++) {
      this.colors.push(this.getRandomColor());
    }
  }

  getRandomColor(): string {
    return '#' + (Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');
  }

  getMonetaryEvloution() {

    let years = new Array();
    let datasetsVal = [];
    let x = new Map();

    if (this.selectedTechnologiesSalaryTech.length > 0) {

      this.selectedTechnologiesSalaryTech.forEach(
        l => {
          let salaries = new Array();
          this.resourceService.getYearEvolution(l.id).subscribe(resp => {
            Array.from(Object.keys(resp["salaryScale"])).forEach(k => {

              if (years.indexOf(k) == -1) {
                years.push(k);
              }
              salaries.push(resp["salaryScale"][k]);
            }, x.set(l, salaries));
          }, error => { }, () => {
            datasetsVal.push(this.getDataset(l.id, x.get(l)));
            this.basicData = {
              labels: this.sortNumber(years),
              datasets: datasetsVal
            }
          });
        }
      )
    } else {
      this.basicData = {
        labels: [],
        datasets: []
      }
    }
  }

  getMonetaryEvloutionByGroup() {
    let yearsGroup= new Array();
    let datasetsValGroup = new Array();
    let x = new Map();
    let techNames = new Array();
    if (this.selectedGroupTechnologiesSalaryTech.length > 0) {
      this.selectedGroupTechnologiesSalaryTech.forEach(t => techNames.push(t.name));

      let salaries = new Array();
      this.resourceService.getYearEvolutionGroup(techNames).subscribe(resp => {
        if (resp != null) {
          Array.from(Object.keys(resp["salaryScale"])).forEach(k => {

            if (yearsGroup.indexOf(k) == -1) {
              yearsGroup.push(k);
            }
            salaries.push(resp["salaryScale"][k]);
          });
        }
      }, error => { }, () => {

        salaries = this.sortNumber(salaries);

        datasetsValGroup.push(this.getDatasetGroup(techNames, salaries));


        this.basicDataGroup = {
          labels: this.sortNumber(yearsGroup),
          datasets: datasetsValGroup
        }
      });
    } else {
      this.basicDataGroup = {
        labels: [],
        datasets: []
      }
    }
  }

  getStadistic() {

    let yearsStadistics = new Array();
    let mode = new Array();
    let media = new Array();
    let median = new Array();
    this.resourceService.getStadistics([this.selectedTechnologiesStadistic[0].name]).subscribe(resp => {
      console.log(resp);
      if (resp != null) {
        console.log(resp["statistics"][1]);
        Array.from(Object.keys(resp["statistics"])).forEach(k => {

          console.log(Object.keys(k))
          if (yearsStadistics.indexOf(k) == -1) {
            yearsStadistics.push(k);
          }

          mode.push(resp["statistics"][k]["mode"]);
          media.push(resp["statistics"][k]["media"]);
          median.push(resp["statistics"][k]["median"]);
        });
      }
    }, error => { }, () => {
      console.log(media);
      this.basicStadistic = {
        labels: this.sortNumber(yearsStadistics),
        datasets: [
          {
            label: 'media',
            backgroundColor: '#42A5F5',
            data: media
          },
          {
            label: 'moda',
            backgroundColor: '#FFA726',
            data: mode
          },
          {
            label: 'mediana',
            backgroundColor: '#334F54',
            data: median
          }
        ]
      }
    });

  }



  getStadisticGroup() {

    let names= new Array();
    let yearsStadistics = new Array();
    let mode = new Array();
    let media = new Array();
    let median = new Array();
    for(let i=0; i<this.selectedTechnologiesStadisticGroup.length; i++){
      names.push(this.selectedTechnologiesStadisticGroup[i].id)
    }

    this.resourceService.getStadistics(names).subscribe(resp => {
      console.log(resp);
      if (resp != null) {
        console.log(resp["statistics"][1]);
        Array.from(Object.keys(resp["statistics"])).forEach(k => {

          console.log(Object.keys(k))
          if (yearsStadistics.indexOf(k) == -1) {
            yearsStadistics.push(k);
          }

          mode.push(resp["statistics"][k]["mode"]);
          media.push(resp["statistics"][k]["media"]);
          median.push(resp["statistics"][k]["median"]);
        });
      }
    }, error => { }, () => {
      console.log(media);
      this.basicStadisticGroup = {
        labels: this.sortNumber(yearsStadistics),
        datasets: [
          {
            label: 'media',
            backgroundColor: '#42A5F5',
            data: media
          },
          {
            label: 'moda',
            backgroundColor: '#FFA726',
            data: mode
          },
          {
            label: 'mediana',
            backgroundColor: '#334F54',
            data: median
          }
        ]
      }
    });

  }



  getDatasetGroup(labels: string[], salary: number[]): any {
    let isdata = {
      label: labels,
      data: salary,
      backgroundColor: this.getRandomColor(),
      hoverBackgroundColor: this.getRandomColor()
    }
    return isdata;
  }

  getDataset(labelName: string, salaries: Array<number>): any {
    let isdata = {
      label: labelName,
      data: this.sortNumber(salaries),
      fill: false,
      borderColor: this.getRandomColor()
    }
    return isdata;
  }

  sortNumber(numArray: Array<any>): Array<number> {
    numArray.sort(function (a, b) {
      return a - b;
    });
    return numArray;
  }


}


