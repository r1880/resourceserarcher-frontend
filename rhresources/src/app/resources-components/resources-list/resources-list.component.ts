import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login/LoginService.service';
import { CV } from 'src/app/services/resource/cv';
import { Resource } from 'src/app/services/resource/Resource';
import { ResourceService } from 'src/app/services/resource/resource.service';
import { SharedResourceService } from 'src/app/services/resource/SharedResourceService';
import { Technology } from 'src/app/services/technology/Technology';
import { TechnologyService } from 'src/app/services/technology/technology.service';
import { saveAs } from "file-saver";
import { ConfirmationService, SelectItem } from 'primeng/api';
import { Language } from 'src/app/services/language/Langauge';
import { LanguageService } from 'src/app/services/language/language.service';




@Component({
  selector: 'app-resources-list',
  templateUrl: './resources-list.component.html',
  styleUrls: ['./resources-list.component.css']
})
export class ResourcesListComponent implements OnInit {


  private resourceService: ResourceService;
  private technologyService: TechnologyService;
  private languageService: LanguageService;
  private sharedResourceService: SharedResourceService;
  private loginService: LoginService;
  private router: Router;
  private confirmationService: ConfirmationService;


  technologies: Technology[];
  languages: Language[];
  resources: Resource[];
  resource: Resource;
  langTarget: Language[];
  techTarget: Technology[];
  yearsTarget: number;
  profileTarget: string;
  sortField: string;
  sortOrder: number;
  sortOptions: SelectItem[];





  constructor(loginService: LoginService, resourceService: ResourceService, router: Router, sharedResourceService: SharedResourceService, private cdr: ChangeDetectorRef, technologyService: TechnologyService, languageService: LanguageService, confirmationService: ConfirmationService) {
    this.loginService = loginService;
    this.router = router;
    this.loginService.isLogged(this.router);
    this.resourceService = resourceService;
    this.router = router;
    this.technologyService = technologyService;
    this.sharedResourceService = sharedResourceService;
    this.confirmationService = confirmationService;
    this.languageService = languageService;


  }


  ngOnInit(): void {
    this.sortOptions = [
      { label: 'Descendente', value: '!salary' },
      { label: 'Ascendente', value: 'salary' }
    ];
    this.technologyService.getTechnologies().subscribe(t => this.technologies = t);
    this.languageService.getLanguages().subscribe(l => this.languages = l);
    this.resourceService.getResources().subscribe(r => {
      this.resources = r;
    });
  }

  update(resource: Resource) {
    this.sharedResourceService.setResource(resource);
    this.router.navigateByUrl('/update-resource');
  }

  download(cv: CV) {
    this.resourceService.downloadFile(cv).subscribe((resp) => {
      saveAs(resp, cv.name);

    });;
  }

  delete(resource: Resource) {
    this.resourceService.removeResource(resource).subscribe(e => { console.log(e) });;
  }


  search() {
    this.resource = new Resource();
    this.resource.profile = this.profileTarget;
    this.resource.yearsOfExperience = this.yearsTarget;
    this.resource.technologies = this.techTarget;
    this.resource.languages = this.langTarget;
    this.resourceService.findResource(this.resource).subscribe(r => {
      this.resources = r;
    });
  }


  confirm(resource: Resource) {
    this.confirmationService.confirm({
      acceptLabel: "Si",
      message: '¿Eliminar al recurso ' + resource.name + ' ' + resource.surname + '?',
      accept: () => {
        this.delete(resource);
      }
    });
  }

  onSortChange(event) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    }
    else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }

}
