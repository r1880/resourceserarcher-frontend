FROM nginx:stable-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY localhost.crt /etc/ssl/certs
COPY localhost.key /etc/ssl/private
COPY rhresources/sources /usr/share/nginx/html