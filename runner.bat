docker container stop rrhh-front
docker container rm rrhh-front
docker image rm rrhh-front:1.0.0
docker build -t rrhh-front:1.0.0 .
docker run -it -d --name="rrhh-front" --network HUMAN_RESOURCES -p 443:443 rrhh-front:1.0.0
